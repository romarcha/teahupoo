# Try to find Teahupoo
#
# Once done this will define the following variables
#   TEAHUPOO_FOUND - System has Kobold
#   TEAHUPOO_INCLUDE_DIRS - The Kobold include directories
#   TEAHUPOO_LIBRARIES - The libraries needed to use Kobold
#   TEAHUPOO_DEFINITIONS - Compiler switches required for using Kobold


find_path(
    TEAHUPOO_INCLUDE_DIR teahupoo/teahupoo.hpp
    PATHS
        ${TEAHUPOO_ROOT}/include
)
find_library(
    TEAHUPOO_LIBRARY teahupoo
    PATHS
        ${TEAHUPOO_ROOT}/lib
)

set(
    TEAHUPOO_INCLUDE_DIRS
    ${TEAHUPOO_INCLUDE_DIR}
)

set(
    TEAHUPOO_LIBRARIES
    ${TEAHUPOO_LIBRARY}
)

# Handle the QUIETLY and REQUIRED arguments and set LIBXML2_FOUND to TRUE if
# all listed variables are TRUE
include( FindPackageHandleStandardArgs )
find_package_handle_standard_args(
    TEAHUPOO DEFAULT_MSG
    TEAHUPOO_LIBRARY TEAHUPOO_INCLUDE_DIR
)
    
mark_as_advanced( TEAHUPOO_LIBRARY TEAHUPOO_INCLUDE_DIR )
