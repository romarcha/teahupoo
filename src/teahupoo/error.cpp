#include <teahupoo/error.h>

namespace teahupoo
{

BaseError::BaseError(std::string const& what)
:   std::runtime_error(what)
{}

}
