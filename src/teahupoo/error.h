#ifndef __TEAHUPOO_ERROR_H__
#define __TEAHUPOO_ERROR_H__

#include <stdexcept>

namespace teahupoo {
    
class BaseError : public std::runtime_error
{
    public:
        BaseError(std::string const& what);
};

} /* kobold */


#endif /* __KOBOLD_ERROR_H__ */
