#ifndef __TEAHUPOO_HPP__
#define __TEAHUPOO_HPP__

namespace teahupoo
{

static const int MAJOR_VERSION = 0;
static const int MINOR_VERSION = 1;

} /* teahupoo */

#endif /* __TEAHUPOO_HPP__ */
