add_executable( ex_file_listing file_listing.cpp )
target_link_libraries( ex_file_listing kobold )

add_executable( ex_fitting fitting.cpp )
target_link_libraries( ex_fitting kobold )

add_executable( ex_gen_data gen_data.cpp )
target_link_libraries( ex_gen_data kobold )

add_executable( ex_image_dataset image_dataset.cpp )
target_link_libraries( ex_image_dataset kobold )

add_executable( ex_image_patches image_patches.cpp )
target_link_libraries( ex_image_patches kobold )

add_executable( ex_integer_hash integer_hash.cpp )
target_link_libraries( ex_integer_hash kobold )

add_executable( ex_logger logger.cpp )
target_link_libraries( ex_logger kobold )

add_executable( ex_lsh lsh.cpp )
target_link_libraries( ex_lsh kobold )

add_executable( ex_parameters parameters.cpp )
target_link_libraries( ex_parameters kobold )

add_executable( ex_file_read_write file_read_write.cpp )
target_link_libraries( ex_file_read_write kobold )

add_executable( ex_slic slic.cpp )
target_link_libraries( ex_slic kobold )

add_executable( ex_vfs vfs.cpp )
target_link_libraries( ex_vfs kobold )
